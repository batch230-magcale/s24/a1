// console.log("hello");

const num1 = 2;
const getCube = num1 ** 3;
console.log(`The cube of ${num1} is ${getCube}`);

// ----------------------------------------------------------------


const address = ["258 Washington Ave NW", "California", 90011];
const [houseNumber, state, postalCode] = address
console.log(`I live at ${houseNumber}, ${state} ${postalCode}`);

const animal = {
		animalName: "Lolong",
		animalType: "saltwater crocodile",
		animalWeight: 1075,
		animalSize: "20 ft 3 in"
	}

const {animalName, animalType, animalWeight, animalSize} = animal;
console.log(`${animalName} was a ${animalType}. He weighed at ${animalWeight} kgs with a measurement of ${animalSize}.`);


// ----------------------------------------------------------------

const printNumbers = [1, 2, 3, 4, 5];
printNumbers.forEach((printNumbers) => {
	console.log(printNumbers);
})

let reduceNumber = () => {
    result = printNumbers.reduce((acc, curr) => acc + curr);
    console.log(result);
}
reduceNumber();



// ----------------------------------------------------------------

class Dog {
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

// Instantiating an object
const dog = new Dog("Frankie", 5, "Miniature Dachshund");
console.log(dog);
